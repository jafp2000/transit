import React, { useState } from 'react'
import Link from 'next/link'
import Table from './components/Table'

const style = {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
}

const style2 = {
    display: 'flex',
    flex: 1,
    flexDirection: 'row'
}

const source = [
    {
        group: 'Grupo A',
        items: [
            'Brasil',
            'Bolivia',
            'Venezuela',
            'Peru'
        ]
    },
    {
        group: 'Grupo B',
        items: [
            'Argentina',
            'Colombia',
            'Paraguay',
            'Qatar'
        ]
    },
    {
        group: 'Grupo C',
        items: [
            'Uruguay',
            'Ecuador',
            'Japon',
            'Chile'
        ]
    }
]

const App = () => {
    const [data, setData] = useState(source)

    return (
        <div style={style}>
            <h1>Copa America</h1>
            <div style={style2}>
                {
                    data.map((item, index) => (
                        <Table key={index} group={item.group} items={item.items}/>
                    ))
                }
            </div>
            <Link href='/test'>
                <a>Go To</a>
            </Link>
        </div>
    )
}

export default App