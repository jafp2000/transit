import React from 'react'

const style = {
    display: 'flex',
    flex: '1',
    flexDirection: 'column',
    paddingRight: '10px',
    paddingLeft: '10px'
}

const Table = ({group, items}) => {
    return (
        <div style={style}>
            <div style={{ width: '300px', backgroundColor: 'gray'}}>
                <h2>{group}</h2>
            </div>
            {
                items.map((item, index) => (
                    <div key={index}>
                        <h3>{item}</h3>
                    </div>
                ))
            }
        </div>
    )
}

export default Table