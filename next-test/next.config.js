const {
    PHASE_DEVELOPMENT_SERVER,
    PHASE_PRODUCTION_BUILD
  } = require('next/constants')

  module.exports = phase => {
    // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
    const isDev = phase === PHASE_DEVELOPMENT_SERVER
    // when `next build` or `npm run build` is used
    const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1'
    // when `next build` or `npm run build` is used
    const isStaging = PHASE_PRODUCTION_BUILD && process.env.STAGING === '1'

    console.log(`isDev:${isDev}  isProd:${isProd}   isStaging:${isStaging}`)

    const env = {
        REACT_APP_API_URL: (() => {
        if (isDev) return 'http://localhost:8080'
        if (isProd) return 'http://localhost:8080'
        if (isStaging) return 'http://localhost:8080'
        return 'RESTURL_SPEAKERS:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
        REACT_APP_GOOGLE_MAP_KEY: (() => {
            if (isDev) return 'AIzaSyASdqEa_TiJh82_xwQjo5jjNwmT4kvAAgQ'
            if (isProd) return 'AIzaSyASdqEa_TiJh82_xwQjo5jjNwmT4kvAAgQ'
            if (isStaging) return 'AIzaSyASdqEa_TiJh82_xwQjo5jjNwmT4kvAAgQ'
            return 'RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
        REACT_APP_GOOGLE_MAP_LANG: (() => {
            if (isDev) return 'es'
            if (isProd) return 'es'
            if (isStaging) return 'es'
            return 'es'
        })()
    }

    return {
      env,
      target: 'serverless',
      assetPrefix: 'http://localhost:5000'
    }
  }