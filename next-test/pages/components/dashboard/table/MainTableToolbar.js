import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import createStyles from '@material-ui/core/styles/createStyles'
import Toolbar from '@material-ui/core/Toolbar'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PlaceIcon from '@material-ui/icons/Place';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { AppContext } from '../Context'

const styles = (theme) =>
  createStyles({
    root: {
      flex: 1,
      flexDirection: 'column',
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    tabs: {
      flex: '1'
    },
    search: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end'
    },
    searchBox: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: '45%',
    },
    input: {
      marginLeft: 8,
      flex: 1,
    }
});

class MainTableToolbar extends React.Component {
    render() {
      const { classes } = this.props

      const { tab, searchValue } = this.context

      return (
        <Toolbar
          className={classes.root}
        >
          <div className={classes.tabs}>
            <Tabs
                value={tab}
                onChange={this.context._handleTab}
                centered
            >
                <Tab
                    disableRipple
                    icon={<PlaceIcon />}
                    label="Routes"
                />
                <Tab
                    disableRipple
                    icon={<FavoriteIcon color="secondary"/>}
                    label="Favorites"
                />
            </Tabs>
          </div>
          <div className={classes.search}>
            <Paper className={classes.searchBox}>
              <InputBase
                className={classes.input}
                value={searchValue}
                onChange={ e => this.context._onSearch(e.target.value)}
                placeholder="Search"
              />
              <SearchIcon/>
            </Paper>
          </div>
        </Toolbar>
      )
    }
}

MainTableToolbar.contextType = AppContext

export default withStyles(styles)(MainTableToolbar)