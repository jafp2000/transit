import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import createStyles from '@material-ui/core/styles/createStyles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import MainTableHead from './MainTableHead'
import MainTableToolbar from './MainTableToolbar'
import { AppContext } from '../Context'
import IconButton from '@material-ui/core/IconButton'
import FavoriteIcon from '@material-ui/icons/Favorite'
import NavigationIcon from '@material-ui/icons/Navigation'
import RemoveIcon from '@material-ui/icons/RemoveCircle'

const styles = (theme) =>
  createStyles({
    root: {
        width: '100%',
        marginTop: theme.spacing(1),
      },
      paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
      },
      table: {
        minWidth: 640,
      },
      tableWrapper: {
        overflowX: 'auto',
      },
  });

class MainTable extends React.Component {
    constructor(){
        super();

        this.state = {
            order: 'asc',
            orderBy: 'route_id',
            page: 0,
            rowsPerPage: 5
        }
    }

    handleRequestSort = (event, property) => {
        const isDesc = this.state.orderBy === property && this.state.order === 'desc'

        this.setState({order: isDesc ? 'asc' : 'desc'})

        this.setState({orderBy: property})
    }

    handleChangePage = (event, newPage) => {
        this.setState({page: newPage})
    }

    handleChangeRowsPerPage = (event) => {
        this.setState({rowsPerPage: event.target.value})
    }

    stableSort = (array, cmp) => {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
          const order = cmp(a[0], b[0]);
          if (order !== 0) return order;
          return a[1] - b[1];
        });
        return stabilizedThis.map(el => el[0]);
    }

    desc(a, b, orderBy) {
        if (b[orderBy] < a[orderBy]) {
          return -1;
        }
        if (b[orderBy] > a[orderBy]) {
          return 1;
        }
        return 0;
    }

    getSorting = (order, orderBy) => {
        return order === 'desc' ? (a, b) => this.desc(a, b, orderBy) : (a, b) => - this.desc(a, b, orderBy);
    }

    checkFavorite(row){
      let exist = false

      this.context.favorites.forEach(item => {
        if(item.route_id === row.route_id)
            exist = true
      })

      return exist
    }

    render() {
        const { order, orderBy, page, rowsPerPage } = this.state

        const { classes } = this.props

        const { routesHeader, tab, rows, selected } = this.context

        const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

        return (
            <div className={classes.root}>
              <Paper className={classes.paper}>
                <MainTableToolbar />
                <div className={classes.tableWrapper}>
                  <Table
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    size='medium'
                  >
                    <MainTableHead
                      headRows={routesHeader}
                      order={order}
                      orderBy={orderBy}
                      onRequestSort={this.handleRequestSort}
                    />
                    <TableBody>
                      {this.stableSort(rows, this.getSorting(order, orderBy))
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row, index) => {
                          return (
                            <TableRow
                              hover
                              tabIndex={-1}
                              key={index}
                            >
                              <TableCell component="th" scope="row">
                                {row.route_id}
                              </TableCell>
                              <TableCell component="th" scope="row">{row.agency_id}</TableCell>
                              <TableCell component="th" scope="row">{row.route_long_name}</TableCell>
                              <TableCell component="th" scope="row">
                                <div>
                                  <IconButton onClick={() => this.context._handleSelected(row)}>
                                    <NavigationIcon
                                      color={
                                        row.route_id === selected.route_id ?
                                          "primary" :
                                          "default"
                                      }
                                    />
                                  </IconButton>
                                  {
                                    tab === 0 ?
                                    <IconButton onClick={() => this.context._addToFavorites(row)}>
                                      <FavoriteIcon
                                        color={
                                          this.checkFavorite(row) ?
                                            "secondary" :
                                            "default"
                                        }
                                      />
                                    </IconButton>
                                    :
                                    <IconButton onClick={() => this.context._removeToFavorites(row)}>
                                      <RemoveIcon color="action"/>
                                    </IconButton>
                                  }

                                </div>
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      {emptyRows > 0 && (
                        <TableRow style={{ height: 49 * emptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                  </Table>
                </div>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component="div"
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  backIconButtonProps={{
                    'aria-label': 'Previous Page',
                  }}
                  nextIconButtonProps={{
                    'aria-label': 'Next Page',
                  }}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
              </Paper>
            </div>
          );
    }
}

MainTable.contextType = AppContext

export default withStyles(styles)(MainTable)