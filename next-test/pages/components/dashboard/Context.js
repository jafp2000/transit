import React from 'react'
import { filter } from '../../helpers/Filter'
import { getRoutes, getTrips } from '../../services'

export const AppContext = React.createContext({
    routes: [],
    trips: [],
    routesHeader: [],
    mapInitialPosition: {lat: -33.4372, lng: -70.6506},
    mapLanguage: 'es',
    mapLibraries: [],
    mapZoom: 16
});

export class DashboardContextProvider extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tab: 0,
            rows: [],
            routes: [],
            trips: [],
            favorites: [],
            searchValue: '',
            selected: {},
            routesHeader: [
                { id: 'route_id', numeric: false, disablePadding: false, label: 'ID' },
                { id: 'agency_id', numeric: false, disablePadding: false, label: 'Agency' },
                { id: 'route_long_name', numeric: false, disablePadding: false, label: 'Name' }
            ],
            mapInitialPosition: {lat: -33.4372, lng: -70.6506},
            mapZoom: 11,
            loading: true,
            isRendering: true
        };
    }

    componentWillMount(){
        this.setState({loading: true})

        getRoutes().then(res => {
            if(res.code === 200){
                this.setState({routes: res.data, rows: res.data, loading: false})
            }else {
                console.log(res.message)
            }
        })
    }

    handleSelected = (item) => {
        this.setState({loading: true, isRendering: true, selected: item})

        getTrips(item.route_id).then(res => {
            if(res.code === 200){
                console.log(res)
                this.setState({trips: res.data, loading: false})
            }else {
                console.log(res.message)
            }
        })
    }

    handleTab = (event, value) => {
        this.setState({isRendering: false})

        let newRows = value === 0 ? this.state.routes : this.state.favorites

        if(this.state.searchValue !== ''){
            newRows = filter(this.state.searchValue, newRows)
        }

        this.setState({rows: newRows, tab: value})
    }

    addToFavorites = (route) => {
        this.setState({isRendering: false})

        let exist = false

        this.state.favorites.forEach(item => {
           if(item.route_id === route.route_id)
                exist = true
        })

        if(!exist){
            const newFavs = this.state.favorites

            newFavs.push(route)

            this.setState({favorites: newFavs})
        }else{
            this.removeToFavorites(route)
        }
    }

    removeToFavorites = (route) => {
        this.setState({isRendering: false})

        let index = -1

        this.state.favorites.forEach((item, i) => {
           if(item.route_id === route.route_id)
                index = i
        })

        if(index !== -1){
            const newFavs = this.state.favorites

            newFavs.splice(index, 1)

            this.setState({favorites: newFavs})
        }
    }

    onSearch = (value) => {
        this.setState({isRendering: false})

        let newRows = this.state.tab === 0 ? this.state.routes : this.state.favorites

        newRows = filter(value, newRows)

        this.setState({rows: newRows, searchValue: value})
    }

    render() {
        const { children } = this.props;

        return (
            <AppContext.Provider
                value={{
                    ...this.state,
                    _handleTab: this.handleTab,
                    _addToFavorites: this.addToFavorites,
                    _handleSelected: this.handleSelected,
                    _removeToFavorites: this.removeToFavorites,
                    _onSearch: this.onSearch
                }}
            >
                {children}
            </AppContext.Provider>
        );
    }
}

export const AppContextConsumer = AppContext.Consumer;