import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import createStyles from '@material-ui/core/styles/createStyles'
import CircularProgress from '@material-ui/core/CircularProgress';
import loadGoogleMapsAPI from 'load-google-maps-api'
import { AppContext } from '../Context'

const styles = (Theme) =>
  createStyles({
    root: {
        display: 'flex',
        flex: 1,
        padding: 10
    },
    loading: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

class Map extends React.Component {

    componentDidUpdate(){
        loadGoogleMapsAPI({
            key: process.env.REACT_APP_GOOGLE_MAP_KEY,
            language: process.env.REACT_APP_GOOGLE_MAP_LANG,
            libraries: []
        }).then( googleMaps => {
            if(this.context.isRendering){
                this.map = new googleMaps.Map( this.refs.map, {
                    center: this.context.mapInitialPosition,
                    zoom: this.context.mapZoom
                });

                if(this.context.trips.shapes){
                    const color = this.context.trips.route_color || 'FF0000'

                    for (let i in this.context.trips.shapes) {
                        const item = this.context.trips.shapes[i]

                        var lineCoordinates = []

                        for (let j in item){
                            const shape = item[j]

                            lineCoordinates.push({
                                lat: parseFloat(shape.shape_pt_lat),
                                lng: parseFloat(shape.shape_pt_lon)
                            })
                        }

                        var temp = new googleMaps.Polyline({
                            path: lineCoordinates,
                            geodesic: true,
                            strokeColor: `#${color}`,
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });

                        temp.setMap(this.map)
                    }
                }
            }
          }).catch( err => {
            console.error(err);
          });
    }

    render() {
        const { classes, width, height} = this.props

        return (
            <div className={classes.root}>
                {
                    this.context.loading ?
                        <div className={classes.loading} style={{width, height}}>
                            <CircularProgress/>
                        </div>
                        :
                        <React.Fragment>
                            <div ref="map" style={{width, height}}>
                                loading map...
                            </div>
                        </React.Fragment>
                }
            </div>
        )
    }
}

Map.contextType = AppContext

export default withStyles(styles)(Map)