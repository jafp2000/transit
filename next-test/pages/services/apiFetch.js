import fetch from 'isomorphic-fetch';

export function apiFetch(endpoint, options = {}) {
  const getPromise = async () => {
    try {
      const fetchOptions = apiOptions(options)
      const fetchEndpoint = `${process.env.REACT_APP_API_URL}${endpoint}`
      const response = await fetch(fetchEndpoint, fetchOptions)
      return response.json()
    } catch (e) {
      throw e
    }
  };

  return getPromise()
}

export function apiOptions(options = {}) {
  const {
    method = 'GET',
    headers = {
      'Content-Type': 'application/json'
    },
    body = false
  } = options;

  const newOptions = {
    method,
    headers
  };

  if (body) {
    newOptions.body = headers['Content-Type'] ? JSON.stringify(body) : body;
  }

  return newOptions;
}
