import React from 'react';
import Container from './containers'
import Dashboard from './components/dashboard'

class App extends React.Component {
  render() {
    return (
      <Container>
        <Dashboard/>
      </Container>
    )
  }
}

export default App;