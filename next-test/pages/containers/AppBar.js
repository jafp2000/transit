import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { createStyles, withStyles } from '@material-ui/core/styles';

const styles = (theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    bar: {
        backgroundColor: '#EBEEF6'
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    }
  });

const MainAppBar = ({classes}) => {
    return (
      <div className={classes.root}>
        <AppBar className={classes.bar} position="static">
          <Toolbar>
              <img alt="" src="transit.png"/>
          </Toolbar>
        </AppBar>
      </div>
    );
}

export default withStyles(styles)(MainAppBar);