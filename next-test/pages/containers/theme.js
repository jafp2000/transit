import { createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';


export const theme = createMuiTheme({
  palette: {
    primary: {
        light: '#757ce8',
        main: '#4377a7',
        dark: '#fff',
        contrastText: '#fff',
    },
    secondary: red
  },

  overrides: {
    MuiDrawer: {
      paper: {
        background: '#cecece',
      },
    },
  },


});