export const filter = (value, arr) => {
    const temp = Object.assign([], arr);

    const regex = new RegExp(value, "i")

    const result = temp.filter(item => (regex.test(item.route_id) || regex.test(item.route_long_name) || regex.test(item.agency_id)))

    return result
}