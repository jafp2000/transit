const xlsxtojson = require("xlsx-to-json-lc");

exports.loadRoutes = function(req, res, next){
    try {
        xlsxtojson({
            input: './source/routes.xlsx',
            output: null,
            lowerCaseHeaders: true
        }, function(err, result) {
            if (err) {
                throw err
            }

            req.routes = result

            next()
        })
    }
    catch (e) {
        throw e
    }
}

exports.loadTrips = function(req, res, next){
    try {
        xlsxtojson({
            input: './source/trips.xlsx',
            output: null,
            lowerCaseHeaders: true
        }, function(err, result) {
            if (err) {
                throw err
            }

            req.trips = result

            next()
        });
    }
    catch (e) {
        throw e
    }
}