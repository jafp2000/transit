require('dotenv/config');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const xlsxtojson = require("xlsx-to-json-lc");
const dataHandling = require('./middlewares/dataHandling')

let app = express();

const port = process.env.PORT || 8080;

app.use(cors());
app.use(bodyParser.json());
app.use(dataHandling.loadRoutes)
app.use(dataHandling.loadTrips)

app.get('/api/routes', (req, res) => {
    res.status(200).json({ code: 200, data: req.routes });
});

app.get('/api/trips/:id', (req, res) => {
    try {
        const route = req.routes.filter(item => item.route_id === req.params.id)[0]

        const trips = req.trips.filter(item => item.route_id === req.params.id)

        xlsxtojson({
            input: './source/shapes.xlsx',
            output: null,
            lowerCaseHeaders: true
        }, function(err, result) {
            if (err) {
                return res.status(500).json({ code: 500, message: err });
            }

            var shapes = []

            trips.forEach(trip => {
                var temp = result.filter(item => item.shape_id === trip.shape_id)

                shapes.push(temp)
            });

            res.status(200).json({ code: 200, data: {route, shapes} });
        });
    }
    catch (e) {
        console.log(e)
        return res.status(400).json({ code: 400, message: "Corupted excel file" });
    }
});

app.listen(port, () => {
    console.log(`server running in port ${port}`)
});

module.exports = app;