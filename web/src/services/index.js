import { apiFetch } from './apiFetch'

export const getRoutes = () => apiFetch(`/api/routes`)
export const getTrips = (id) => apiFetch(`/api/trips/${id}`)