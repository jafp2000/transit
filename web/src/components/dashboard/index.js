import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import createStyles from '@material-ui/core/styles/createStyles'
import Grid from '@material-ui/core/Grid'
import Table from './table'
import Map from './map'
import { DashboardContextProvider } from './Context'


const styles = (Theme) =>
  createStyles({
    root: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        backgroundColor: "#F7F9FF"
    }
  });


class Dashboard extends React.Component {

  render() {
    const { classes } = this.props;

    return (
      <DashboardContextProvider>
        <Grid container className={classes.root} spacing={2}>
              <Grid item lg="auto" md={6} sm={12}>
                  <Table/>
              </Grid>
              <Grid item lg="auto" md={6} sm={12}>
                  <Map width={600} height={500}/>
              </Grid>
        </Grid>
      </DashboardContextProvider>
    );
  }
}

export default withStyles(styles)(Dashboard)