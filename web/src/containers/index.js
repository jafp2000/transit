import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import { theme } from './theme'
import createStyles from '@material-ui/core/styles/createStyles';
import Card from '@material-ui/core/Card';
import AppBar from './AppBar';

const styles = (Theme) =>
  createStyles({
    root: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        padding: 10,
        margin: 10
    }
  });


class Container extends React.Component {

  render() {
    const { children, classes } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        <div className={classes.root}>
            <AppBar/>
            <Card className={classes.content}>
                {children}
            </Card>
        </div>
      </MuiThemeProvider>

    );
  }
}

export default withStyles(styles, { withTheme: true })(Container);